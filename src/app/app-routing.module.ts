import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { BlankComponent } from './blank/blank.component';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  {path: '', component: FormComponent},
  {path: 'thank-you', component: BlankComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
