import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild('f') userForm!: NgForm;

  formUploading = new Subject<boolean>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,) { }

  ngOnInit(): void {
  }


  join() {
    console.log(this.userForm.value.userName);
    const body = {
      userName: this.userForm.value.userName,
      surname: this.userForm.value.surname,
      middleName: this.userForm.value.middleName,
      phone: this.userForm.value.phone,
      job: this.userForm.value.job,
      tshirt: this.userForm.value.tshirt,
      size: this.userForm.value.size,
      comment: this.userForm.value.comment
    };


    this.formUploading.next(true);

    return this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/users.json', body).pipe(
      tap(() => {
        this.formUploading.next(false);
        void this.router.navigate(['/thank-you'], {relativeTo: this.route})
      }, () => {
        this.formUploading.next(false);
      })
    ).subscribe();
  }
}
